# Ereignis/Edition

<!-- Titel --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.p) 

Ich freue mich, hier darstellen zu dürfen, was ich in den ersten fünf Jahren meiner Tätigkeit an der ÖAW für das Institute for Habsburg and Balkan Studies getan habe, woran ich aktuell arbeite und wie eine Zukunft des Instituts unter meiner Mitwirkung und mit meiner Expertise aussehen könnte. Ich picke etwas heraus, an dem ich glaube einige Dinge zeigen zu können: Es geht mir heute um Ereignisse, und um die Geschichte einer Reparatur.

<!-- Chronofotografie --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g283cd1ab50c_0_21) 

Ich steige mit einem Beispiel ein: 

Nehmen wir an, wir wollen als Ereignis kodieren, was hier geschieht. Es gibt ein Rahmenereignis der Aufnahme des Bildes, es gibt dann einzelne Ereignisse, bei denen der Verschluss öffnet und belichtet wird. Als Personen beteiligt sind an dem Ereignis zumindest ein Fotograf und der Springer. Der Ort ließe sich wohl herausfinden und, wenn man das möchte, auch mit Geokoordinaten bezeichnen. Bezüge zu Vorgänger- und Nachfolgeereignissen aus der Chronofotografie wären kodierbar, Sie können sich das vorstellen. Anderes Beispiel. 

<!-- tei:event non validating --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g2820d5aa7d1_0_0) 
Sie sehen die formalisierte Beschreibung einer Ministerratssitzung als Ereignis. Das Unangenehme sind nur die Unterwellungen des Editors: gewisse Elemente wären nicht erlaubt. In dem Element `<event>` ist keine Liste von Personen oder von Orten erlaubt, und das Element `<eventName>` ist dem Modell überhaupt unbekannt. Dieses Problem hat ganz konkret vor vier Jahren Überlegungen angestoßen, deren Lösung ich heute vorstellen möchte. Die Frage ist, wie wir <!-- tei:event validating --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g2820d5aa7d1_0_6) dahin kommen, dass die Validierung in dem Community Standard der Text Encoding Initiative klappt, und darum soll es gehen.

<!-- Gliederung --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g281fe04003b_3_0) 

Mein Vortrag hat drei Kapitel: 

- Die Geschichte der Reparatur von `<tei:event>`, 
- Beispiele aus der Edition am Institut, und zum Schluss kurz 
- Pläne für damit verbundene weitere Forschung.

----

<!-- Kap 1 tei:event --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_45) 

## Modellierung

<!-- Ereignis/event --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_67) 

Erstes Kapitel: Ereignis oder `<event>`. Was ist ein Ereignis? 

- Die meisten deutschsprachigen Wörterbücher verwenden in ihren Definitionen das Bemerkenswerte: etwas Bemerkenswertes muss zuerst bemerkt werden, das heißt: Es geht in Übereinstimmung mit der Etymologie des Wortes um Zeugenschaft, um ein Geschehen, das sich vor den Augen anderer er-äugnet.
- Explizit ausklammern möchte ich hier die reichhaltige geschichtswissenschaftliche Diskussion, was ein geschichtliches Ereignis konstituiert. Dies ist nur ein Subset von Ereignis, und hier geht es unbescheiden um das große Ganze. 

<!-- tei:event @4.6.0 --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_75) 
Die Guidelines der Text Encoding Initiative, mit denen ich in meiner Editions- und Datenmodellierungstätigkeit täglich zu tun habe, beschreiben in der noch aktuellen Version P5 v.4.6.0 das Element `<event>` mit "contains data relating to any kind of significant event associated with a person, place, or organization." 

Daran problematisch ist neben der zirkulären Definition vor allem die Fixierung auf Personen, Plätze und Organisationen, die sich darin niederschlägt, dass `event` nur als Kindelement von `person`, `place` und `org` vorkommen darf, also eine Person z.B. einem Institut angehören, ein Werk verfassen in Bezug zu einer bibliographischen Einheit, reisen in Bezug zu mehreren Plätzen o.ä. kann. Problematisch ist weiters, dass es kein diesen Elementen entsprechendes Element zur Verwendung im Text gibt: kein `<eventName>` in Analogie zu `<orgName>`, `<placeName>` oder `<persName>`.

<!-- tei2019 --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_104) 
2019 hat sich zur TEI-Jahreskonferenz in Graz eine Gruppe von Kolleginnen und Kollegen gefunden, die an unterschiedlichen digitalen Editionen beteiligt sind: ein mittelalterliches Itinerar von Helmut Klug, damals am ZIM in Graz, das Tagebuch des Literaten und Funktionärs Andreas Okopenko an der Österreichischen Nationalbibliothek, vertreten von Christoph Steindl und Christiane Fritze, und ich mit damals etwa zweieinhalbtausend retrodigitalisierten Protokollen des Ministerrats 1848–67. Wir haben zu dem Problemaufriss, dass das Datenmodell von `event` deutlichen Reparaturbedarf hat, auch einen Anwendungsfall für die Neumodellierung ausgearbeitet, nämlich gemeinsame Kalender mit minimalen Metadaten zu den betreffenden Quelldokumenten befüllbar zu machen. Überwunden wurden in der Folge technische Herausforderungen, aber auch damit verbundene logistische und soziale. 

<!-- jTEI Beitrag --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_111) 
Den konkreten Weg dieser Reparatur möchte ich in einem Beitrag für das Journal der TEI noch genauer ausarbeiten.

Trotz verschiedener Widerstände, vor allem betraf das das Zeitbudget, folgte technische Umsetzung und Überzeugungsarbeit. Vier Jahre nach der ursprünglichen Diskussion ist es nun soweit, dass in den nächsten Release der Guidelines die mehrfach ausführlich diskutierten Änderungen hineinkommen. 

<!-- tei:event @4.7.0a --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_84) 
Das heißt im Konkreten: 

- Es gibt eine neue Definition: "contains data relating to anything of significance that happens in time."
- `listEvent` und `event` dürfen auch in `object` verwendet werden
- im Text dürfen Ereignisnamen mit dem neuen Element `eventName` bezeichnet werden, das selbstverständlich auch ergänzend zu `head` und `label` in `event` verwendet werden kann
- `ptr` und `idno` sind als Kindelemente von `event` gestattet
- Generell ist `event` damit entkoppelt von Personen und Orten und kann in Analogie zu sämtlichen anderen Elementen verwendet werden, die benannte Entitäten beschreiben. 

Das sind minimalinvasive Eingriffe in das Gebäude der TEI-Guidelines, die drei der rund 700 Elemente betrafen, letztlich war das eine kleine Reparatur. 

Eine kleine Reparatur, aber sie ist folgenreich: Es geht um die Formalisierung einer Beschreibungskategorie, die letztlich *alle* Editionsprojekte betrifft, die die TEI anwenden; und in den meisten Editionen, allen voran jenen aus den Geschichtswissenschaften, spielen Ereignisse einer Rolle. Dies ist auch unabhängig davon, ob XML verwendet wird oder eine andere Technologie: Die TEI bietet ein etabliertes, sozial kontrolliertes Vokabular und Definitionen von Elementinhalten und Bezügen, die sich z.B. auch für Text als Graph eignen, wie das Andreas Kuczera vorgeschlagen hat. 

Mein Aufgabenbereich am IHB ist die Edition und ich habe derzeit z.B. mit Computational Literary Studies, mit Stylometrie und mit Verfahren der automationsgestützten Textanalyse nur am Rande zu tun, auch wenn ich zumindest für die  Ministerratsprotokolle auch Pläne für ein Analyseprojekt habe. 

<!-- event Anwendungsfälle --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_131) 

Ein Vorteil daran, innerhalb der TEI ohne Anpassungen Ereignisse als vollwertige Entitäten darstellen zu können, liegt in der Kompatibilität des reparierten Elements mit Datenmodellen aus der Prosopographie. 

Formalisiert am weitesten fortgeschritten ist sicher das *International Prosopographical Interchange Framework (IPIF)*, das grundsätzlich quads, also tripelförmige Statements und eine zusätzliche Attribuierung baut, aber das wäre Georg Vogelers Vortrag. Die strukturierten Daten, die wir am IHB haben, sind entweder tabellarischer Form, oder wir pflegen sie in Instanzen des *Austrian Prosopographical Information Systems* APIS, wo temporalisierte Entitäten und Relationen abgelegt sind. APIS stammt aus der Zeit, als das *Österreichische Biographische Lexikon* Teil unseres Instituts war, und es ist ein gutes Beispiel für wunderbare Zusammenarbeit zwischen Instituten an der ÖAW, in dem Fall mit dem ACDH-CH.

Ein Anwendungsfall, an dem unser Forschungsbereich *Digitale Historiographie und Editionen* beteiligt sein wird, ist die gemeinsame Ressource, die derzeit mit dem ACDH-CH und dem Institut für Mittelalterforschung aufgebaut wird: Ein kuratierter Wissensgraph, der neben der Identifikation und Disambiguierung von Entitäten, die bspw. aus unseren APIS-Instanzen kommen, auch Anschlussfähigkeit dieser Daten an die Linked Open Data-Welt gewährleistet. Das geschieht, indem die Daten mit Klassen aus dem CIDOC-CRM in RDF serialisiert werden. Die auf Personen, Organisationen, Orte, Werke und Ereignisse bezogenen Faktoide aus unterschiedlichen Projekten werden damit sinnvoller nachnutzbar; sie lassen sich mit dem reparierten `<event>` auch alle in valides TEI exportieren. Es wird damit deutlich weniger Reibungsverlust geben zwischen den verschiedenen Formaten, und die Projekte, die strukturierte Daten sammeln und bearbeiten, werden gemeinsam an einem Strang ziehen können. 

<!-- APIS instanzen --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_137) 

Die erforderliche Interoperabilität betrifft bei uns am Institut neben unzähligen Informationen zu den Ministerien der cisleithanischen Reichshälfte auch die Daten, die am Forschungsbereich Habsburgermonarchie aus dem Projekt *The Viennese Court. A prosopographical Portal* die Hofzahlamtsbücher erfasst haben, die Registerdaten zum Briefwechsel der Kaiserin Eleonore Magdalena, jene zu den Quellen zur habsburgisch-osmanischen Diplomatie, und darüber hinaus die entstehenden Daten zum Projekt GraViz, das das Amt des Osmanischen Großvezierats über Jahrhunderte verfolgt. Vor einigen Wochen hat mein Kollege Petr Maťa sein Evaluationsverfahren mit einem Vortrag zu einem Projekt bestritten, das die Sitzungen der Landtage aller Kronländer erfassen soll. Sie sehen: Es gibt eine Menge von Ereignissen, die für jemanden relevant sind und die maschinenlesbar beschrieben werden sollen.

Was für diese und weitere Forschungsunternehmen jeweils "ereignisfähig" ist, muss auf der Ebene der abstrakten Modellierung offen bleiben; die Reparatur des `event`-Elements im Namensraum TEI ist jedenfalls vorerst erledigt. 

Auch wenn wir als Forschungsbereich in den vergangenen fünf Jahren etwa dreieinhalbtausend Ministerratsprotokolle und knapp dreihundert diplomatische Quellen als XML-Daten frei verfügbar gemacht haben, glaube ich, dass meine Arbeit an diesem Element in diesem Community Standard TEI den größten "Impact" haben wird, den ich allerdings nur schwer auf eine Publikationsliste setzen kann.

----

<!-- Konkretion: Beispiele --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_54) 

## Konkretion

Zweites Kapitel: Vom Abstrakten zum Konkreten: Lassen Sie mich das noch aus meinem Arbeitsbereich am IHB illustrieren, die zugleich Absichtserklärungen und Einladungen zur Diskussion sind. Sie kennen beide Projekte aus den Unterlagen.

<!-- QhoD --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_58) 

### QhoD

Für das im Jahr 2020 begonnene Projekt QhoD habe ich nicht nur Datenkonversion und Verarbeitung gemacht, sondern auch eine Edition in TEI vorbereitet: <!-- Driesch --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_144)  Cornelius von den Driesch begleitet die Gesandtschaft von Damian Hugo von Virmondt im Jahr 1719 an den Hof nach Konstantinopel. Sein Reisebericht bietet für das Projekt zu diesem Großbotschafterwechsel das chronologisch-ereignisgeschichtliche Skelett. Über `event`s im obigen Sinn werden wir Audienzen, Treffen, Reisestationen annotieren, die auch in anderen Quellen vorkommen und damit ein Ereignisregister erstellen. Technisch lässt sich das durch das kleinteilige Markup in dem Projekt durch XQuery-Abfragen nach Gleichzeitigkeiten in `date`-Elementen vorbearbeiten. 

Der ehemalige Jesuit Driesch ist auch als Person und als Autor von dramatischen Texten hochinteressant, hier plane ich gemeinsam auch mit germanistischen KollegInnen einen Workshop für nächstes Jahr. 

<!-- QhoD Itinerar --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g2868b3e35f6_0_0) 

Hier ein Beispiel, wie eine `<listEvent>` zu einem im *Wienerischen Diarium* abgedruckten Itinerar einer Großbotschaft aussehen könnte. Sie sehen den Vorteil, strukturiert abfragen zu können, wer z.B. wann in Niš gewesen ist. 

<!-- QhoD Quellen --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_153) 

Mit der semantischen Erschließung von `date` zu `event` werden wir die Auffindbarkeit und Benutzbarkeit der gesammelten Quellen deutlich steigern. Für dieses Projekt haben wir eine Gesamterschließung, die auch temporal organisiert ist; Dafür, dass nur zwei Personen daran nur zwei Jahre gearbeitet haben, es sind Manuela Mayer und Yasir Yilmaz, sind 280 Datensätze schon eine erkleckliche Anzahl an edierten Quellen. 

<!-- QhoD Ottoman --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_160) 

Für das Osmanische, das ich selbst nicht lese, wird sich durch ein Modelltraining für Transkribus der Output steigern lassen; hier stehe ich im Austausch mit anderen Projekten, die den selben Problemkomplex vorfinden: Nicht vokalisiertes arabopersisches Alphabet, verschiedene Schreibtraditionen der einzelnen Kanzleien, hohe Lehnwortdichte aus dem Persischen und Arabischen haben gemeinsam mit einer analogen Lesekultur der Osmanistik dafür gesorgt, dass kaum frei verfügbare Trainingsdaten existieren, mit denen man Modelle füttern kann. Wir arbeiten daran. 

<!-- MRP --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_62) 

### MRP

<!-- calendar.html --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_171) 

Im Bereich der Protokolleditionen haben wir die Sitzungsdaten, also Ort, Zeit, Anwesende und eine Liste der Tagungsordnungspunkte, die sich recht einfach als `event`s herausziehen lassen. Die JSON-Serialisierung des Kalenders der MRP-Webapplikation funktioniert genau so. Drei Bemerkungen: 

<!-- protokolleditionen.eu --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_178) 

1. Erstens habe ich mit KollegInnen aus verwandten Editionen den *Arbeitskreis Protokolleditionen* gegründet, der sich halbjährlich zum Austausch auf inhaltlicher Ebene, auf Ebene der Workflows und des technischen Setups trifft. In Planung ist ein gemeinsamer Kalender, der v.a. für Editionen mit temporalen Überlappungen spannend wird. `event`s aus einer Klasse von Editionen werden dort zusammenkommen. <!-- eventSearch --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_98) Die KollegInnen von der Nationalbibliothek haben auch Vorarbeiten zum Design der API für eine *eventSearch*-Lösung geleistet, das ist ein abgeschlossenes CLARIAH-AT-Projekt. 

<!-- CMR VIII/1 --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_186) 

2. Ein Ereignis, das noch diesen Herbst eintreten wird, ist die hybride Publikation des ersten Teilbandes zum Weltkriegsband 1914–1918 der cisleithanischen Ministerratsprotokolle, bearbeitet von Wladimir Fischer-Nebmaier; der Output nach LaTeX ist gerade in der letzten Korrekturphase. 

<!-- Projektantrag --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g281fe04003b_0_0) 

3. Bei den retrodigitalisierten ersten 28 Bänden der Edition hat das in den letzten Jahren stark geschrumpfte Team keine Zeitressourcen, um Ereignisse manuell nachzuerfassen oder sich in automatische Verfahren einzuarbeiten. Deswegen plane ich einen FWF-Projektantrag, der genau das zum Gegenstand machen wird: Wie bekommen wir named entities aus einem nur leicht strukturierten Volltext heraus und machen diesen damit besser analysierbar?^[1] Das Korpus der Ministerratsprotokolle ist sicher über die politische Geschichte hinaus interessant und lohnt sich auszuwerten und bekannter zu machen. 

<!-- Ereignis schafft Erkenntnis --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g280448ae6c0_0_3) 

## Zusammenfassung Pläne

Drittes, kürzestes Kapitel: Das waren nur einige Beispiele aus meiner Domäne, aber letztlich wollte ich zeigen, wie das funktioniert, die TEI-Guidelines soweit aufzubohren, dass Ereignisse aus beliebigen Zusammenhängen hineinpassen. Damit ist die Grundlage dafür geschaffen, Daten aus anderen Quellen besser nachnutzen und bearbeiten zu können.

Ich hoffe, dass Sie aus diesem heutigen Ereignis die Erkenntnis mitnehmen, dass die Modellierung von Ereignissen tatsächlich auch neue Erkenntnisse erzeugen kann.

<!-- Pläne --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g1d3dd5979ca77d92_1) 
Zum Schluss habe ich nochmals die Dinge zusammengefasst, die über meinen aktuellen Alltag und Auftrag in der Unterstützung und Bearbeitung hinausgehen. 

Das geplante Projekt, das das MRP-Korpus weiter erschließt und neue Analyse- und Visualisierungswege erarbeitet, würde mir erlauben, auch ein kleines Team am Institut aufzubauen, das dann auch für das Prosopographieprojekt zur Verfügung steht und mich bei der Kooperation mit unserem Schwesterinstitut ACDH-CH unterstützt. 

In Bezug auf die Verbesserung der Handschriftenerkennung für das Osmanische bin ich sicher, dass es lohnt, hier Zeit zu investieren und der Community bei der Werkzeugentwicklung und beim Erzeugen von Ground Truth beizustehen. 

Sonst möchte ich wie bisher meine praktischen Erfahrungen auch publizieren, unter anderem wie gesagt mit einem Bericht zur heute besprochenen Reparatur. 

In der Coda hätte ich dann auch noch eine Folie, die das in den Gesamtzusammenhang des Instituts einordnet, aber damit möchte ich es hier einmal bewenden lassen. 

<!-- </event> --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g27eb2c76fa4_0_193) 
## `</event>` 

Vielen Dank für die Aufmerksamkeit und ich freue mich auf Fragen und Diskussion! 

<!-- Publikationsliste --> [▮](https://docs.google.com/presentation/d/1LQMLBR5EZ4egi7E_CBURYOMoCPKMWh643ocbUs5n1N4/edit#slide=id.g283cd1ab50c_0_0) 