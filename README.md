# Ereignis/Edition

Repo für Codebeispiele, Folien und Vortragsmanuskript, Vortrag zur Evaluation an der ÖAW, 2023-10-05. 

Slides: <https://tinyurl.com/eval-kurz-2023> (google presentation), [Ereignis-Edition-Slides.md](./Ereignis-Edition-Slides.md) (markdown)

Vortragsmanuskript: [Ereignis-Edition-Vortragsmanuskript.md](./Ereignis-Edition-Vortragsmanuskript.md)

<https://orcid.org/0000-0003-2546-2570>

