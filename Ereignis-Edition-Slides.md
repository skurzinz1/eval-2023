# Ereignis/Edition Slides

stephan.kurz@oeaw.ac.at	
2023-10-05
Folien: https://tinyurl.com/eval-kurz-2023 

## Ereignis

https://upload.wikimedia.org/wikipedia/commons/a/a3/Weitsprung_mit_Anlauf_1886.jpg
Ereignis »Sequenz«  
»Verschlussöffnung«  
»Verschlussöffnung«  
»Verschlussöffnung«  
»Verschlussöffnung«  
»Verschlussöffnung«  
»Verschlussöffnung«  
Person: »der Springer«  
Person: »der Fotoingenieur«  
Ort: Geokoordinaten der Aufnahme?   
Ereignisbezug: Vorgänger/Nachfolger in der Chronofotografie…

## https://gitlab.com/skurzinz1/eval-2023/-/blob/main/mrp-event-example.xml, nicht validierend

## https://gitlab.com/skurzinz1/eval-2023/-/blob/main/mrp-event-example.xml, validierend

## Gliederung

1. Modellierung tei:event Motivation, Problemstellung und erfolgreicher Verlauf einer Reparatur
2. Konkretion Beispiele aus den Editionen des IHB QhoD | MRP
3. Schlüsse, Pläne

## I Modellierung

tei:event
`<event>`

### Ereignis/event Begriffsbestimmung 

https://www.oed.com/dictionary/event / https://www.dwds.de/wb/Ereignis / Brockhaus in 4 Bd. 1937

### tei:event @4.6.0
https://tei-c.org/release/doc/tei-p5-doc/en/html/ref-event.html

~~eventName~~

### TEI 2019 Graz / DHd2020 Paderborn

https://doi.org/10.5281/zenodo.3447297
- TEI-Guidelines als Infrastruktur
- Mittelalterliches Itinerar
- Tagebucheinträge Okopenko
- Ministerratssitzungen

https://doi.org/10.5281/zenodo.8366436  
Panel zur breiteren Diskussion

### Beitrag für das jTEI 

Gliederung: 
- Motivation: den Standard und seine Kompatibilität reparieren und verbessern
- technische und infrastrukturelle Erfordernisse
- Formalisierung in ODD 
- ›Social hacking‹: 
  - die richtigen Personen zum richtigen Zeitpunkt ansprechen und sie überzeugen
  - Nutzung der institutionell verankerten Kommunikationskanäle (mailing list) und Werkzeuge (GitHub pull request)
- entlang der Neumodellierung von tei:event and tei:eventName

### tei:event / tei:eventName @4.7.0a

https://bauman.zapto.org/~syd/temp/tei/Guidelines-web/en/html/ref-event.html

### Anwendungsfälle

- Erschließung von Ereignissen in TEI-Editionen
- prosopographische Daten
- interoperabel zwischen tei:event und RDF
- Föderation in ÖAW-weiter Ressource »Prosopographic Research Portal« (PRP) (ACDH-CH, IMAFO, IHB)

### Anwendungsfälle

prosopographische Daten am IHB
- https://mpr.acdh.oeaw.ac.at 
- https://viecpro.oeaw.ac.at 
- https://github.com/emt-project/emt-entities/tree/main/indices
- https://qhod.net 
- GraViz
- Landtage
- legacy data aus anderen Projekten

Föderation in ÖAW-weiter Ressource (ACDH-CH, IMAFO, IHB) »Prosopographic Research Portal« (PRP)

## II Konkretion

Beispiele

### qhod.net

Digitale Edition von Quellen zur habsburgisch-osmanischen Diplomatie 1500–1918

#### QhoD Driesch, Historische Nachricht

https://qhod.net/o:vipa.tr.hbg.1723 

### QhoD events 

http://gams-staging.uni-graz.at/o:vipa.pp.hbg.17190613/sdef:TEI/get?mode=view:facs#facs_15 

Beispiel https://gitlab.com/skurzinz1/eval-2023/-/blob/main/qhod-event-example.xml

#### QhoD Events in der Chronologie der Quellenübersicht 

https://qhod.net/archive/objects/context:qhod/methods/sdef:Context/get?mode=table 


#### QhoD Probleme des Osmanischen, HTR

https://qhod.net/o:vipa.pr.osm.17200414/sdef:TEI/get?mode=view:facs

### mrp.oeaw.ac.at

Die Ministerratsprotokolle Österreichs und der österreichisch-ungarischen Monarchie 1848–1918

#### Kalenderdaten als tei:event 

https://mrp.oeaw.ac.at/pages/calendar.html?year=1867 

#### protokolleditionen.eu

https://teimec2023.uni-paderborn.de/contributions/144.html  
Panel, DHd2022: https://doi.org/10.5281/zenodo.6304589 

#### EventSearch API, DARIAH-AT-Projekt 

https://edition.onb.ac.at/o:ode.EventSearchInfo 

#### PDF-Druckfahnen zur hybriden Edition von CMR Band VIII/1

#### MRP-Erschließungsprojekt (geplant)

- 28 Bde./5 Mio Word-Token, nur strukturerschlossen
- Gouvernementales Wissen 1848–1867
- Topic Modelling, Topics auf der Zeitachse
- Abgleich mit Parallelkorpora (stenogr. Prot. Reichsrat)
- Abgleich mit Folgekorpora (Cisleithanischer MR; ris.gv.at) 
- Verfolgen politischer Materien über Zeit

Ziel: Erschließung, Visualisierungsmöglichkeiten, erhöhte Nutzbarkeit für nichtakademisches Publikum

## III Schlüsse, Pläne

Ereignis schafft Erkenntnis

### `<listEvent from="2024-02-01">` (Pläne)

- Editionen weiter betreuen und erweitern 
- Serialisierung von `<event>` aus APIS-Datenbanken
- MRP Projekt zu Analyse und Visualisierung (FWF-Antrag)
- Prosopographiedaten in PRP (ACDH-CH, IMAFO, IHB)
- mit GraViz/QhoD: Ottoman HTR
- DH Fachdiskurs/Publikationen
  - »Starting from a mess« (mit Elisa Beshero-Bondar)
  - jTEI (tei:event)

## `</event>`

Danke für die Aufmerksamkeit!

## … seit Beginn des Evaluationsverfahrens

Proposal for event and eventName. Text Encoding Initiative Guidelines Pull request #2427 (2023). 

mit Yasir Yılmaz: QhoD: Digitale Edition von Quellen zur Habsburgisch-Osmanischen Diplomatie 1500–1918, in: Journal of the Ottoman and Turkish Studies Association (JOTSA), 9,2, 2022, 57–65  

Administrativ-politische und editorisch-technische Protokolle. Der cisleithanische Ministerrat 1848–1918, in: Plener Peter, Niels Werber und Burkhardt Wolf (eds.): Das Protokoll, AdminiStudies 2, Springer/Metzler 2023, S. 93–110  

Workshop Ohge/Thomas, Digital Scholarly Editions: Critical Opportunities (2023-03)

### … für diesen Vortrag

mit Sven Jüngerkes, Towards shared TEI model/s for institutional minutes and protocols – protokolleditionen.eu, Poster, TEIMEC2023 Paderborn (https://doi.org/10.5281/zenodo.8359627) 

mit Fritze Christiane; Klug Helmut W.; Schlögl Matthias; Steindl Christoph, Panel: Events: Modellierungen und Schnittstellen (DHd 2020, Paderborn, 2020). (https://doi.org/10.5281/zenodo.3666690)

mit Fritze Christiane; Klug Helmut W.; Steindl Christoph, Recreating history through events (TEI 2019, Graz, 2019). (https://gams.uni-graz.at/o:tei2019.141)


Codebeispiele, Redemanuskript: https://gitlab.com/skurzinz1/eval-2023.git 

## `<back>`

## Integration innerhalb des IHB

(geht fast jeder Antrag auch über meinen Tisch)

FB Balkanforschung
- Visual Archive Southeastern Europe (GAMS, LIDO)
- Hammer-Purgstall/Moldau diplomatische Korrespondenz
- Linguistic History of Albanian Place Names (HAPA) (ACDH-CH)

FB Kunstgeschichte
- Archäologiegeschichte, Amtsgebäude (FAIRe Daten strukturiert in MPR zu übernehmen)

FB Habsburgermonarchie
- Prosopographische Daten zum Wiener Hof (VieCPro, Der Wiener Hof: Eliten, Herrschaft und Repräsentation)
- Fuggerzeitungen-Projekt und QhoD-Bezüge (Christoph Neumann)
- Karten- und Statistikbände Reihe *Die Habsburgermonarchie*

übergreifend
- Themenplattform ›Räume und Herrschaft‹ Bosnien-Workshop Feb 2024
https://habsmon.hypotheses.org/

## Event: Weitere Beispiele

- Generisch: Was als Ereignis modelliert wird
- Das ist erkenntnisinteressengetrieben!
- Filmkader
- »der Moment, als X die Idee hatte«
- literarische Handlungsteile / fiktive Ereignisse 
- edge case: »der Urknall« (which happens in before at the start of time)
- Timeline https://maechtekongresse.acdh.oeaw.ac.at/pages/timeline.html
- …
